/**
 * Скрипт для подписывания данных с помощью плагина крипто-про
 * 
 * Last updated: 28.03.2016 Version 4.0
 * 
 * Исправлено: в IE теперь список сертификатов грузится с первого раза
 * 
 */
var constants = {
	CERT_CHOOSER : "certChooser",
	CERT_TABLE_BODY : "certTableBody",
	SUBJECT_COLUMN_WIDTH : 200,
	ISSUER_COLUMN_WIDTH : 230,
	VALIDDATE_COLUMN_WIDTH : 120
};
var highlightInvalidCertificates = false;
var oddRow = true;
var isPluginEnabled = false;

//var fileContent; // Переменная для хранения информации из файла, значение
// присваивается в cades_bes_file.html
function getXmlHttp() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
		xmlhttp = new XMLHttpRequest();
	}
	return xmlhttp;
}

var async_code_included = 0;
var async_Promise;
var async_resolve;
function include_async_code() {
	if (async_code_included) {
		return async_Promise;
	}
	var fileref = document.createElement('script');
	fileref.setAttribute("type", "text/javascript");
	fileref.setAttribute("src", "async_code.js");
	document.getElementsByTagName("head")[0].appendChild(fileref);
	async_Promise = new Promise(function(resolve, reject) {
		async_resolve = resolve;
	});
	async_code_included = 1;
	return async_Promise;
}

function Common_RetrieveCertificate() {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return RetrieveCertificate_Async();
		});
	} else {
		return RetrieveCertificate_NPAPI();
	}
}

function Common_CreateSimpleSign(id) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return CreateSimpleSign_Async(id);
		});
	} else {
		return CreateSimpleSign_NPAPI(id);
	}
}

function Common_SignCadesBES(id, text, setDisplayData) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return SignCadesBES_Async(id, text, setDisplayData);
		});
	} else {
		return SignCadesBES_NPAPI(id, text, setDisplayData);
	}
}

function Common_SignCadesBES_File(id) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return SignCadesBES_Async_File(id);
		});
	} else {
		return SignCadesBES_NPAPI_File(id);
	}
}

function Common_SignCadesXLong(id) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return SignCadesXLong_Async(id);
		});
	} else {
		return SignCadesXLong_NPAPI(id);
	}
}

function Common_SignCadesXML(id) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return SignCadesXML_Async(id);
		});
	} else {
		return SignCadesXML_NPAPI(id);
	}
}

function Common_CheckForPlugIn() {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return CheckForPlugIn_Async();
		});
	} else {
		return CheckForPlugIn_NPAPI();
	}
}

function Common_Encrypt() {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return Encrypt_Async();
		});
	} else {
		return Encrypt_NPAPI();
	}
}

function Common_Decrypt(id) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return Decrypt_Async(id);
		});
	} else {
		return Decrypt_NPAPI(id);
	}
}

function GetCertificateByThumbprint_NPAPI(certId) {

	var thumbprint = certId.split(" ").reverse().join(
			"").replace(/\s/g, "").toUpperCase();
	try {
		var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
		oStore.Open();
	} catch (err) {
		alert('Failed to create CAdESCOM.Store: ' + GetErrorMessage(err));
		return;
	}

	var CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0;
	var oCerts = oStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_SHA1_HASH,
			thumbprint);

	if (oCerts.Count == 0) {
		alert("Certificate not found");
		return;
	}
	var oCert = oCerts.Item(1);
	return oCert;
}

/*function GetCertificate_NPAPI(certListBoxId) {
	var e = document.getElementById(certListBoxId);
	var selectedCertID = e.selectedIndex;
	if (selectedCertID == -1) {
		alert("Select certificate");
		return;
	}

	var thumbprint = e.options[selectedCertID].value.split(" ").reverse().join(
			"").replace(/\s/g, "").toUpperCase();
	try {
		var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
		oStore.Open();
	} catch (err) {
		alert('Failed to create CAdESCOM.Store: ' + GetErrorMessage(err));
		return;
	}

	var CAPICOM_CERTIFICATE_FIND_SHA1_HASH = 0;
	var oCerts = oStore.Certificates.Find(CAPICOM_CERTIFICATE_FIND_SHA1_HASH,
			thumbprint);

	if (oCerts.Count == 0) {
		alert("Certificate not found");
		return;
	}
	var oCert = oCerts.Item(1);
	return oCert;
}*/

function FillCertInfo_NPAPI(certificate, certBoxID) {
	var BoxID;
	var field_prefix;
	if (typeof (certBoxID) == 'undefined') {
		BoxID = 'cert_info';
		field_prefix = '';
	} else {
		BoxID = certBoxID;
		field_prefix = certBoxID;
	}

	var certObj = new CertificateObj(certificate);
	document.getElementById(BoxID).style.display = '';
	document.getElementById(field_prefix + "subject").innerHTML = "Владелец: <b>" + certObj.GetCertName() + "<b>";
	document.getElementById(field_prefix + "issuer").innerHTML = "Издатель: <b>" + certObj.GetIssuer() + "<b>";
	document.getElementById(field_prefix + "from").innerHTML = "Выдан: <b>" + certObj.GetCertFromDate() + "<b>";
	document.getElementById(field_prefix + "till").innerHTML = "Действителен до: <b>" + certObj.GetCertTillDate() + "<b>";
	document.getElementById(field_prefix + "provname").innerHTML = "Криптопровайдер: <b>" + certObj.GetPrivateKeyProviderName() + "<b>";
	document.getElementById(field_prefix + "algorithm").innerHTML = "Алгоритм ключа: <b>" + certObj.GetPubKeyAlgorithm() + "<b>";
}

/**
 * Подписание данных для IE
 * @param dataToSign
 * @param certObject
 * @param setDisplayData
 * @returns
 */
function MakeCadesBesSign_NPAPI(dataToSign, certObject, setDisplayData) {
	var errormes = "";

	try {
		var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
	} catch (err) {
		errormes = "Failed to create CAdESCOM.CPSigner: " + err.number;
		alert(errormes);
		throw errormes;
	}

	if (oSigner) {
		oSigner.Certificate = certObject;
	} else {
		errormes = "Failed to create CAdESCOM.CPSigner";
		alert(errormes);
		throw errormes;
	}

	try {
		var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
	} catch (err) {
		alert('Failed to create CAdESCOM.CadesSignedData: ' + err.number);
		return;
	}

	var CADES_BES = 1;
	var signature;

	if (dataToSign) {
		// Данные на подпись ввели
		oSignedData.ContentEncoding = 1; // CADESCOM_BASE64_TO_BINARY
		if (typeof (setDisplayData) != 'undefined') {
			// Set display data flag flag for devices like Rutoken PinPad
			oSignedData.DisplayData = 1;
		}
		oSignedData.Content = Base64.encode(dataToSign);
		oSigner.Options = 1; // CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN
		try {
			signature = oSignedData.SignCades(oSigner, CADES_BES);
			return signature;
		} catch (err) {
			errormes = "Не удалось создать подпись из-за ошибки: "
					+ GetErrorMessage(err);
			alert(errormes);
			throw errormes;
		}
	}
}


function MakeCadesXLongSign_NPAPI(dataToSign, tspService, certObject) {
	var errormes = "";

	try {
		var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
	} catch (err) {
		errormes = "Failed to create CAdESCOM.CPSigner: " + err.number;
		alert(errormes);
		throw errormes;
	}

	if (oSigner) {
		oSigner.Certificate = certObject;
	} else {
		errormes = "Failed to create CAdESCOM.CPSigner";
		alert(errormes);
		throw errormes;
	}

	try {
		var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
	} catch (err) {
		alert('Failed to create CAdESCOM.CadesSignedData: ' + GetErrorMessage(err));
		return;
	}

	var CADESCOM_CADES_X_LONG_TYPE_1 = 0x5d;
	var signature;

	if (dataToSign) {
		// Данные на подпись ввели
		oSignedData.Content = dataToSign;
		oSigner.Options = 1; // CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN
		oSigner.TSAAddress = tspService;
		try {
			signature = oSignedData.SignCades(oSigner, CADESCOM_CADES_X_LONG_TYPE_1);
			return signature;
		} catch (err) {
			errormes = "Не удалось создать подпись из-за ошибки: " + GetErrorMessage(err);
			alert(errormes);
			throw errormes;
		}
	}
}

function MakeXMLSign_NPAPI(dataToSign, certObject) {
	try {
		var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
	} catch (err) {
		errormes = "Failed to create CAdESCOM.CPSigner: " + err.number;
		alert(errormes);
		throw errormes;
	}

	if (oSigner) {
		oSigner.Certificate = certObject;
	} else {
		errormes = "Failed to create CAdESCOM.CPSigner";
		alert(errormes);
		throw errormes;
	}

	var XmlDsigGost3410Url = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr34102001-gostr3411";
	var XmlDsigGost3411Url = "urn:ietf:params:xml:ns:cpxmlsec:algorithms:gostr3411";
	var CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED = 0;

	try {
		var oSignedXML = cadesplugin.CreateObject("CAdESCOM.SignedXML");
	} catch (err) {
		alert('Failed to create CAdESCOM.SignedXML: ' + GetErrorMessage(err));
		return;
	}

	oSignedXML.Content = dataToSign;
	oSignedXML.SignatureType = CADESCOM_XML_SIGNATURE_TYPE_ENVELOPED;
	oSignedXML.SignatureMethod = XmlDsigGost3410Url;
	oSignedXML.DigestMethod = XmlDsigGost3411Url;

	var sSignedMessage = "";
	try {
		sSignedMessage = oSignedXML.Sign(oSigner);
	} catch (err) {
		errormes = "Не удалось создать подпись из-за ошибки: "
				+ GetErrorMessage(err);
		alert(errormes);
		throw errormes;
	}

	return sSignedMessage;
}

function GetSignatureTitleElement() {
	var elementSignatureTitle = null;
	var x = document.getElementsByName("SignatureTitle");

	if (x.length == 0) {
		elementSignatureTitle = document.getElementById("SignatureTxtBox").parentNode.previousSibling;

		if (elementSignatureTitle.nodeName == "P") {
			return elementSignatureTitle;
		}
	} else {
		elementSignatureTitle = x[0];
	}

	return elementSignatureTitle;
}

/**
 * Подписание данных для IE
 * @param certId
 * @param data
 * @param setDisplayData
 */
function NewSignCadesBES_NPAPI(certId, data, setDisplayData) {
	var certificate = GetCertificateByThumbprint_NPAPI(certId);
	if (typeof (data) != 'undefined') {
		dataToSign = data;
	}
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var signature = MakeCadesBesSign_NPAPI(dataToSign, certificate,	setDisplayData);
		document.getElementById("SignatureTxtBox").innerHTML = signature;
		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}


/*function SignCadesBES_NPAPI(certListBoxId, data, setDisplayData) {
	var certificate = GetCertificate_NPAPI(certListBoxId);
	if (typeof (data) != 'undefined') {
		dataToSign = data;
	}
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var signature = MakeCadesBesSign_NPAPI(dataToSign, certificate,
				setDisplayData);
		document.getElementById("SignatureTxtBox").innerHTML = signature;
		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}/*

function SignCadesBES_NPAPI_File(certListBoxId) {
	var certificate = GetCertificate_NPAPI(certListBoxId);
	var dataToSign = fileContent;
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var StartTime = Date.now();
		var signature = MakeCadesBesSign_NPAPI(dataToSign, certificate);
		var EndTime = Date.now();
		document.getElementsByName('TimeTitle')[0].innerHTML = "Время выполнения: "
				+ (EndTime - StartTime) + " мс";
		document.getElementById("SignatureTxtBox").innerHTML = signature;
		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}

/*function SignCadesXLong_NPAPI(certListBoxId) {
	var certificate = GetCertificate_NPAPI(certListBoxId);
	var dataToSign = document.getElementById("DataToSignTxtBox").value;
	var tspService = document.getElementById("TSPServiceTxtBox").value;
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var signature = MakeCadesXLongSign_NPAPI(dataToSign, tspService,
				certificate);
		document.getElementById("SignatureTxtBox").innerHTML = signature;
		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}

function SignCadesXML_NPAPI(certListBoxId) {
	var certificate = GetCertificate_NPAPI(certListBoxId);
	var dataToSign = document.getElementById("DataToSignTxtBox").value;
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var signature = MakeXMLSign_NPAPI(dataToSign, certificate);
		document.getElementById("SignatureTxtBox").innerHTML = signature;

		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}
*/
function MakeVersionString(oVer) {
	var strVer;
	if (typeof (oVer) == "string")
		return oVer;
	else
		return oVer.MajorVersion + "." + oVer.MinorVersion + "."
				+ oVer.BuildVersion;
}

function CheckForPlugIn_NPAPI() {
	function VersionCompare_NPAPI(StringVersion, ObjectVersion) {
		if (typeof (ObjectVersion) == "string")
			return -1;
		var arr = StringVersion.split('.');

		if (ObjectVersion.MajorVersion == parseInt(arr[0])) {
			if (ObjectVersion.MinorVersion == parseInt(arr[1])) {
				if (ObjectVersion.BuildVersion == parseInt(arr[2])) {
					return 0;
				} else if (ObjectVersion.BuildVersion < parseInt(arr[2])) {
					return -1;
				}
			} else if (ObjectVersion.MinorVersion < parseInt(arr[1])) {
				return -1;
			}
		} else if (ObjectVersion.MajorVersion < parseInt(arr[0])) {
			return -1;
		}

		return 1;
	}

	function GetCSPVersion_NPAPI() {
		try {
			var oAbout = cadesplugin.CreateObject("CAdESCOM.About");
		} catch (err) {
			alert('Failed to create CAdESCOM.About: ' + GetErrorMessage(err));
			return;
		}
		var ver = oAbout.CSPVersion("", 75);
		return ver.MajorVersion + "." + ver.MinorVersion + "."
				+ ver.BuildVersion;
	}

	function GetCSPName_NPAPI() {
		var sCSPName = "";
		try {
			var oAbout = cadesplugin.CreateObject("CAdESCOM.About");
			sCSPName = oAbout.CSPName(75);

		} catch (err) {
		}
		return sCSPName;
	}

	function ShowCSPVersion_NPAPI(CurrentPluginVersion) {
		if (typeof (CurrentPluginVersion) != "string") {
			document.getElementById('CSPVersionTxt').innerHTML = "Версия криптопровайдера: "
					+ GetCSPVersion_NPAPI();
		}
		var sCSPName = GetCSPName_NPAPI();
		if (sCSPName != "") {
			document.getElementById('CSPNameTxt').innerHTML = "Криптопровайдер: "
					+ sCSPName;
		}
	}
	function GetLatestVersion_NPAPI(CurrentPluginVersion) {
		var xmlhttp = getXmlHttp();
		xmlhttp.open("GET", "https://www.cryptopro.ru/sites/default/files/products/cades/latest_2_0.txt", true);
		xmlhttp.onreadystatechange = function() {
			var PluginBaseVersion;
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					PluginBaseVersion = xmlhttp.responseText;
					if (isPluginWorked) { // плагин работает, объекты
						// создаются
						if (VersionCompare_NPAPI(PluginBaseVersion,
								CurrentPluginVersion) < 0) {
							document.getElementById('PluginEnabledImg')
									.setAttribute("src", "Img/yellow_dot.png");
							document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен, но есть более свежая версия.";
						}
					} else { // плагин не работает, объекты не создаются
						if (isPluginLoaded) { // плагин загружен
							if (!isPluginEnabled) { // плагин загружен, но
								// отключен
								document.getElementById('PluginEnabledImg').setAttribute("src", "Img/red_dot.png");
								document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен, но отключен в настройках браузера.";
							} else { // плагин загружен и включен, но объекты не создаются
								document.getElementById('PluginEnabledImg').setAttribute("src", "Img/red_dot.png");
								document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен, но не удается создать объекты. Проверьте настройки браузера.";
							}
						} else { // плагин не загружен
							document.getElementById('PluginEnabledImg').setAttribute("src", "Img/red_dot.png");
							document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин не загружен.";
						}
					}
				}
			}
		}
		xmlhttp.send(null);
	}

	var isPluginLoaded = false;
	var isPluginWorked = false;
	var isActualVersion = false;
	try {
		var oAbout = cadesplugin.CreateObject("CAdESCOM.About");
		isPluginLoaded = true;
		isPluginEnabled = true;
		isPluginWorked = true;
		// Это значение будет проверяться сервером при загрузке демо-страницы
		var CurrentPluginVersion = oAbout.PluginVersion;
		if (typeof (CurrentPluginVersion) == "undefined")
			CurrentPluginVersion = oAbout.Version;

		document.getElementById('PluginEnabledImg').setAttribute("src", "Img/green_dot.png");
		document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен.";
		document.getElementById('PlugInVersionTxt').innerHTML = "Версия плагина: " + MakeVersionString(CurrentPluginVersion);
		ShowCSPVersion_NPAPI(CurrentPluginVersion);
	} catch (err) {
		// Объект создать не удалось, проверим, установлен ли
		// вообще плагин. Такая возможность есть не во всех браузерах
		var mimetype = navigator.mimeTypes["application/x-cades"];
		if (mimetype) {
			isPluginLoaded = true;
			var plugin = mimetype.enabledPlugin;
			if (plugin) {
				isPluginEnabled = true;
			}
		}
	}
	GetLatestVersion_NPAPI(CurrentPluginVersion);
	if (location.pathname.indexOf("symalgo_sample.html") >= 0) {
		FillCertList_NPAPI('CertListBox1');
		FillCertList_NPAPI('CertListBox2');
	} else {
		//FillCertList_NPAPI('CertListBox');
	}
}

function CertificateObj(certObj) {
	this.cert = certObj;
	this.certFromDate = new Date(this.cert.ValidFromDate);
	this.certTillDate = new Date(this.cert.ValidToDate);
}

CertificateObj.prototype.check = function(digit) {
	return (digit < 10) ? "0" + digit : digit;
};

CertificateObj.prototype.extract = function(from, what) {
	certName = "";

	var begin = from.indexOf(what);

	if (begin >= 0) {
		var end = from.indexOf(', ', begin);
		certName = (end < 0) ? from.substr(begin) : from.substr(begin, end - begin);
	}

	return certName;
};

CertificateObj.prototype.DateTimePutTogether = function(certDate) {
	return this.check(certDate.getUTCDate()) + "."
			+ this.check(certDate.getMonth() + 1) + "."
			+ certDate.getFullYear() + " " + this.check(certDate.getUTCHours())
			+ ":" + this.check(certDate.getUTCMinutes()) + ":"
			+ this.check(certDate.getUTCSeconds());
};

CertificateObj.prototype.GetCertString = function() {
	return this.extract(this.cert.SubjectName, 'CN=') + "; Выдан: "
			+ this.GetCertFromDate();
};

CertificateObj.prototype.GetCertFromDate = function() {
	return this.DateTimePutTogether(this.certFromDate);
};

CertificateObj.prototype.GetCertTillDate = function() {
	return this.DateTimePutTogether(this.certTillDate);
};

CertificateObj.prototype.GetPubKeyAlgorithm = function() {
	return this.cert.PublicKey().Algorithm.FriendlyName;
};

CertificateObj.prototype.GetCertName = function() {
	return this.extract(this.cert.SubjectName, 'CN=');
};

CertificateObj.prototype.GetIssuer = function() {
	return this.extract(this.cert.IssuerName, 'CN=');
};

CertificateObj.prototype.GetPrivateKeyProviderName = function() {
	return this.cert.PrivateKey.ProviderName;
};

/*function GetFirstCert_NPAPI() {
	try {
		var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
		oStore.Open();
	} catch (e) {
		alert("Ошибка при открытии хранилища: " + GetErrorMessage(e));
		return;
	}

	var dateObj = new Date();
	var certCnt;

	try {
		certCnt = oStore.Certificates.Count;
		if (certCnt == 0)
			throw "Cannot find object or property. (0x80092004)";
	} catch (ex) {
		var message = GetErrorMessage(ex);
		if ("Cannot find object or property. (0x80092004)" == message
				|| "oStore.Certificates is undefined" == message
				|| "Объект или свойство не найдено. (0x80092004)" == message) {
			oStore.Close();
			var errormes = document.getElementById("boxdiv").style.display = '';
			return;
		}
	}

	if (certCnt) {
		try {
			for (var i = 1; i <= certCnt; i++) {
				var cert = oStore.Certificates.Item(i);
				if (dateObj < cert.ValidToDate && cert.HasPrivateKey()
						&& cert.IsValid().Result) {
					return cert;
				}
			}
		} catch (ex) {
			alert("Ошибка при перечислении сертификатов: "
					+ GetErrorMessage(ex));
			return;
		}
	}
}*/

/*function CreateSimpleSign_NPAPI() {
	oCert = GetFirstCert_NPAPI();
	var x = GetSignatureTitleElement();
	try {
		if (typeof oCert != "undefined") {
			FillCertInfo_NPAPI(oCert);
			var sSignedData = MakeCadesBesSign_NPAPI(txtDataToSign, oCert);
			document.getElementById("SignatureTxtBox").innerHTML = sSignedData;
			if (x != null) {
				x.innerHTML = "Подпись сформирована успешно:";
			}
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}*/


function FillCertList_NPAPI(lstId) {
	try {
		var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
		oStore.Open();
	} catch (ex) {
		alert("Ошибка при открытии хранилища: " + GetErrorMessage(ex));
		return;
	}

	try {
		var lst = document.getElementById(lstId);
		if (!lst)
			return;
	} catch (ex) {
		return;
	}

	var certCnt;

	try {
		certCnt = oStore.Certificates.Count;
		if (certCnt == 0)
			throw "Cannot find object or property. (0x80092004)";
	} catch (ex) {
		var message = GetErrorMessage(ex);
		if ("Cannot find object or property. (0x80092004)" == message
				|| "oStore.Certificates is undefined" == message
				|| "Объект или свойство не найдено. (0x80092004)" == message) {
			oStore.Close();
			var errormes = document.getElementById("boxdiv").style.display = '';
			return;
		}
	}

	for (var i = 1; i <= certCnt; i++) {
		var cert;
		try {
			cert = oStore.Certificates.Item(i);
		} catch (ex) {
			alert("Ошибка при перечислении сертификатов: "
					+ GetErrorMessage(ex));
			return;
		}

		var oOpt = document.createElement("OPTION");
		var dateObj = new Date();
		try {
			// if(dateObj<cert.ValidToDate && cert.HasPrivateKey() &&
			// cert.IsValid().Result)
			// {
			if (dateObj < cert.ValidToDate && cert.HasPrivateKey()) {
				var certObj = new CertificateObj(cert);
				oOpt.text = certObj.GetCertString();
			} else {
				continue;
			}
		} catch (ex) {
			alert("Ошибка при получении свойства SubjectName: "
					+ GetErrorMessage(ex));
		}
		try {
			oOpt.value = cert.Thumbprint;
		} catch (ex) {
			alert("Ошибка при получении свойства Thumbprint: "
					+ GetErrorMessage(ex));
		}

		lst.options.add(oOpt);
	}

	oStore.Close();
}

function decimalToHexString(number) {
	if (number < 0) {
		number = 0xFFFFFFFF + number + 1;
	}

	return number.toString(16).toUpperCase();
}

function GetErrorMessage(e) {
	var err = e.message;
	if (!err) {
		err = e;
	} else if (e.number) {
		err += " (0x" + decimalToHexString(e.number) + ")";
	}
	return err;
}

/*function CreateCertRequest_NPAPI() {
	try {
		var PrivateKey = cadesplugin
				.CreateObject("X509Enrollment.CX509PrivateKey");
	} catch (e) {
		alert('Failed to create X509Enrollment.CX509PrivateKey: '
				+ GetErrorMessage(e));
		return;
	}

	PrivateKey.ProviderName = "Crypto-Pro GOST R 34.10-2001 Cryptographic Service Provider";
	PrivateKey.ProviderType = 75;
	PrivateKey.KeySpec = 1; // XCN_AT_KEYEXCHANGE

	try {
		var CertificateRequestPkcs10 = cadesplugin
				.CreateObject("X509Enrollment.CX509CertificateRequestPkcs10");
	} catch (e) {
		alert('Failed to create X509Enrollment.CX509CertificateRequestPkcs10: '
				+ GetErrorMessage(e));
		return;
	}

	CertificateRequestPkcs10.InitializeFromPrivateKey(0x1, PrivateKey, "");

	try {
		var DistinguishedName = cadesplugin
				.CreateObject("X509Enrollment.CX500DistinguishedName");
	} catch (e) {
		alert('Failed to create X509Enrollment.CX500DistinguishedName: '
				+ GetErrorMessage(e));
		return;
	}

	var CommonName = "Test Certificate";
	DistinguishedName
			.Encode("CN=\"" + CommonName.replace(/"/g, "\"\"") + "\";");

	CertificateRequestPkcs10.Subject = DistinguishedName;

	var KeyUsageExtension = cadesplugin
			.CreateObject("X509Enrollment.CX509ExtensionKeyUsage");
	var CERT_DATA_ENCIPHERMENT_KEY_USAGE = 0x10;
	var CERT_KEY_ENCIPHERMENT_KEY_USAGE = 0x20;
	var CERT_DIGITAL_SIGNATURE_KEY_USAGE = 0x80;
	var CERT_NON_REPUDIATION_KEY_USAGE = 0x40;

	KeyUsageExtension
			.InitializeEncode(CERT_KEY_ENCIPHERMENT_KEY_USAGE
					| CERT_DATA_ENCIPHERMENT_KEY_USAGE
					| CERT_DIGITAL_SIGNATURE_KEY_USAGE
					| CERT_NON_REPUDIATION_KEY_USAGE);

	CertificateRequestPkcs10.X509Extensions.Add(KeyUsageExtension);

	try {
		var Enroll = cadesplugin.CreateObject("X509Enrollment.CX509Enrollment");
	} catch (e) {
		alert('Failed to create X509Enrollment.CX509Enrollment: '
				+ GetErrorMessage(e));
		return;
	}

	Enroll.InitializeFromRequest(CertificateRequestPkcs10);

	return Enroll.CreateRequest(0x1);
}*/

/*function RetrieveCertificate_NPAPI() {
	var cert_req = CreateCertRequest_NPAPI();
	var params = 'CertRequest=' + encodeURIComponent(cert_req) + '&Mode='
			+ encodeURIComponent('newreq') + '&TargetStoreFlags='
			+ encodeURIComponent('0') + '&SaveCert=' + encodeURIComponent('no');

	var xmlhttp = getXmlHttp();
	xmlhttp.open("POST", "https://www.cryptopro.ru/certsrv/certfnsh.asp", true);
	xmlhttp.setRequestHeader('Content-Type',
			'application/x-www-form-urlencoded');
	var response;
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if (xmlhttp.status == 200) {
				response = xmlhttp.responseText;
				var cert_data = "";

				if (!isIE()) {
					var start = response.indexOf("var sPKCS7");
					var end = response.indexOf("sPKCS7 += \"\"") + 13;
					cert_data = response.substring(start, end);
				} else {
					var start = response.indexOf("sPKCS7 & \"") + 9;
					var end = response.indexOf("& vbNewLine\r\n\r\n</Script>");
					cert_data = response.substring(start, end);
					cert_data = cert_data.replace(new RegExp(" & vbNewLine",
							'g'), ";");
					cert_data = cert_data.replace(new RegExp("&", 'g'), "+");
					cert_data = "var sPKCS7=" + cert_data + ";";
				}

				eval(cert_data);

				try {
					var Enroll = cadesplugin
							.CreateObject("X509Enrollment.CX509Enrollment");
				} catch (e) {
					alert('Failed to create X509Enrollment.CX509Enrollment: '
							+ GetErrorMessage(e));
					return;
				}

				Enroll.Initialize(0x1);
				Enroll.InstallResponse(0, sPKCS7, 0x7, "");
				var errormes = document.getElementById("boxdiv").style.display = 'none';
				if (location.pathname.indexOf("simple") >= 0) {
					location.reload();
				} else if (location.pathname.indexOf("symalgo_sample.html") >= 0) {
					FillCertList_NPAPI('CertListBox1');
					FillCertList_NPAPI('CertListBox2');
				} else {
					//FillCertList_NPAPI('CertListBox');
				}
			}
		}
	}
	xmlhttp.send(params);
}*/

/*function Encrypt_NPAPI() {

	document.getElementById("DataEncryptedIV1").innerHTML = "";
	document.getElementById("DataEncryptedIV2").innerHTML = "";
	document.getElementById("DataEncryptedDiversData1").innerHTML = "";
	document.getElementById("DataEncryptedDiversData2").innerHTML = "";
	document.getElementById("DataEncryptedBox1").innerHTML = "";
	document.getElementById("DataEncryptedBox2").innerHTML = "";
	document.getElementById("DataEncryptedKey1").innerHTML = "";
	document.getElementById("DataEncryptedKey2").innerHTML = "";
	document.getElementById("DataDecryptedBox1").innerHTML = "";
	document.getElementById("DataDecryptedBox2").innerHTML = "";

	var certificate1 = GetCertificate_NPAPI('CertListBox1');
	if (typeof (certificate1) == 'undefined') {
		return;
	}
	var certificate2 = GetCertificate_NPAPI('CertListBox2');
	if (typeof (certificate2) == 'undefined') {
		return;
	}

	var dataToEncr1 = Base64.encode(document
			.getElementById("DataToEncrTxtBox1").value);
	var dataToEncr2 = Base64.encode(document
			.getElementById("DataToEncrTxtBox2").value);

	try {
		FillCertInfo_NPAPI(certificate1, 'cert_info1');
		FillCertInfo_NPAPI(certificate2, 'cert_info2');
		var errormes = "";

		try {
			var oSymAlgo = cadesplugin
					.CreateObject("cadescom.symmetricalgorithm");
		} catch (err) {
			errormes = "Failed to create cadescom.symmetricalgorithm: " + err;
			alert(errormes);
			throw errormes;
		}

		oSymAlgo.GenerateKey();

		var oSesKey1 = oSymAlgo.DiversifyKey();
		var oSesKey1DiversData = oSesKey1.DiversData;
		document.getElementById("DataEncryptedDiversData1").value = oSesKey1DiversData;
		var oSesKey1IV = oSesKey1.IV;
		document.getElementById("DataEncryptedIV1").value = oSesKey1IV;
		var EncryptedData1 = oSesKey1.Encrypt(dataToEncr1, 1);
		document.getElementById("DataEncryptedBox1").value = EncryptedData1;

		var oSesKey2 = oSymAlgo.DiversifyKey();
		var oSesKey2DiversData = oSesKey2.DiversData;
		document.getElementById("DataEncryptedDiversData2").value = oSesKey2DiversData;
		var oSesKey2IV = oSesKey2.IV;
		document.getElementById("DataEncryptedIV2").value = oSesKey2IV;
		var EncryptedData2 = oSesKey2.Encrypt(dataToEncr2, 1);
		document.getElementById("DataEncryptedBox2").value = EncryptedData2;

		var ExportedKey1 = oSymAlgo.ExportKey(certificate1);
		document.getElementById("DataEncryptedKey1").value = ExportedKey1;

		var ExportedKey2 = oSymAlgo.ExportKey(certificate2);
		document.getElementById("DataEncryptedKey2").value = ExportedKey2;

		alert("Данные зашифрованы успешно:");
	} catch (err) {
		alert("Ошибка при шифровании данных:" + err);
	}
}*/

/*function Decrypt_NPAPI(certListBoxId) {

	document.getElementById("DataDecryptedBox1").value = "";
	document.getElementById("DataDecryptedBox2").value = "";

	var certificate = GetCertificate_NPAPI(certListBoxId);
	if (typeof (certificate) == 'undefined') {
		return;
	}
	var dataToDecr1 = document.getElementById("DataEncryptedBox1").value;
	var dataToDecr2 = document.getElementById("DataEncryptedBox2").value;
	var field;
	if (certListBoxId == 'CertListBox1')
		field = "DataEncryptedKey1";
	else
		field = "DataEncryptedKey2";

	var EncryptedKey = document.getElementById(field).value;
	try {
		FillCertInfo_NPAPI(certificate, 'cert_info_decr');
		var errormes = "";

		try {
			var oSymAlgo = cadesplugin
					.CreateObject("cadescom.symmetricalgorithm");
		} catch (err) {
			errormes = "Failed to create cadescom.symmetricalgorithm: " + err;
			alert(errormes);
			throw errormes;
		}
		oSymAlgo.ImportKey(EncryptedKey, certificate);
		var oSesKey1DiversData = document
				.getElementById("DataEncryptedDiversData1").value;
		var oSesKey1IV = document.getElementById("DataEncryptedIV1").value;
		oSymAlgo.DiversData = oSesKey1DiversData;
		var oSesKey1 = oSymAlgo.DiversifyKey();
		oSesKey1.IV = oSesKey1IV;
		var EncryptedData1 = oSesKey1.Decrypt(dataToDecr1, 1);
		document.getElementById("DataDecryptedBox1").value = Base64
				.decode(EncryptedData1);
		var oSesKey2DiversData = document
				.getElementById("DataEncryptedDiversData2").value;
		var oSesKey2IV = document.getElementById("DataEncryptedIV2").value;
		oSymAlgo.DiversData = oSesKey2DiversData;
		var oSesKey2 = oSymAlgo.DiversifyKey();
		oSesKey2.IV = oSesKey2IV;
		var EncryptedData2 = oSesKey2.Decrypt(dataToDecr2, 1);
		document.getElementById("DataDecryptedBox2").value = Base64
				.decode(EncryptedData2);

		alert("Данные расшифрованы успешно:");
	} catch (err) {
		alert("Ошибка при шифровании данных:" + err);
	}
}*/

function isIE() {
	var retVal = (("Microsoft Internet Explorer" == navigator.appName) || // IE <
	// 11
	navigator.userAgent.match(/Trident\/./i)); // IE 11
	return retVal;
}

// -----------------------------------
var Base64 = {

	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	encode : function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output + this._keyStr.charAt(enc1)
					+ this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3)
					+ this._keyStr.charAt(enc4);

		}

		return output;
	},

	decode : function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	_utf8_encode : function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			} else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			} else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	_utf8_decode : function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while (i < utftext.length) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			} else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12)
						| ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}

}
var MakePayment = function(sum, date, to) {
	return '<!PINPADFILE UTF8><N>Платежное поручение<V>500' + '<N>Сумма<V>'
			+ sum + '<N>Дата<V>' + date + '<N>Получатель<V>' + to
			+ '<N>Инн<V>102125125212' + '<N>КПП<V>1254521521'
			+ '<N>Назначение платежа<V>За телематические услуги'
			+ '<N>Банк получателя<V>Сбербанк' + '<N>БИК<V>5005825'
			+ '<N>Номер счета получателя<V>1032221122214422'
			+ '<N>Плательщик<V>ЗАО "Актив-софт"'
			+ '<N>Банк плательщика<V>Банк ВТБ (открытое акционерное общество)'
			+ '<N>БИК<V>044525187'
			+ '<N>Номер счета плательщика<V>30101810700000000187';
};

function ShowPinPadelogin() {
	var loginvalue = document.getElementById('Login').value;
	var text = '<!PINPADFILE UTF8><N>Авторизация<V><N>Подтвердите авторизацию на сайте<V>'
			+ 'cryptopro.ru'
			+ '<N>Вход будет произведен с логином<V>'
			+ loginvalue;
	Common_SignCadesBES('CertListBox', text, 1);
}

/**
 * 
 * ДЛЯ ПРИМЕРА!!!
 * 
 * Запуск подписания. Вызывается из формы выбора прватного ключа
 */
function runSigning(sCertHash) {

	var call = document.getElementById('preparedCall');
	prepareData(JSON.parse(call.value));

	var ok = true;

	log("Signing started...");

	if ("" == sCertHash) {
		logerror("Не выбран сертификат");
		return;
	}

	if (requestTextHash != undefined) {
		log("Signing registry hash running :" + requestTextHash);
		try {
			var signedMessage = signCreate(sCertHash, requestTextHash);
			if (signedMessage != null) {
				ru.alfabank.orrpp.jscallback.setCMS(signedMessage);
				log("Signing registry text done!");
			} else {
				ok = false;
			}
		} catch (e) {
			ok = false;
			logerror(e.message);
			return;
		}
		if (!ok) {
			logerror("Signing failed!");
		}
	} else {
		log("Signing registry skipped, because nothing to sign!");
	}
}

/**
 * Показ окна выбора сертификата для подписания из списка личных сертификатов
 * пользователя.
 */
function showCertChooserNew() {

	var certChooser = document.createElement("div");
	certChooser.setAttribute("id", constants.CERT_CHOOSER);
	certChooser.setAttribute("style","overflow: visible; top: 10%;  left: 10%; height: 80%;  width: 80%; position: absolute; padding-top: 36px; padding-bottom: 9px;z-index: 20000;");
	certChooser.setAttribute("class","v-panel v-widget v-has-width v-has-height");

	// Caption
	var pcaptionwrap = document.createElement("div");
	pcaptionwrap.setAttribute("style", "margin-top: -36px;");
	pcaptionwrap.setAttribute("class", "v-panel-captionwrap");
	certChooser.appendChild(pcaptionwrap);

	var pcaption = document.createElement("div");
	pcaption.setAttribute("class", "v-panel-caption");
	pcaptionwrap.appendChild(pcaption);

	var pcaptionspan = document.createElement("span");
	pcaptionspan.textContent = "Выбор ключа для подписания";
	pcaption.appendChild(pcaptionspan);

	// Content

	var pcontent = document.createElement("div");
	pcontent.setAttribute("class", "v-panel-content v-scrollable");
	pcontent.setAttribute("tabindex", "-1");
	pcontent.setAttribute("style", "position: relative;");
	certChooser.appendChild(pcontent);

	var mainvl = document.createElement("div");
	mainvl.setAttribute("style", "height: 100%; width: 100%;");
	mainvl.setAttribute("class","v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height v-margin-top v-margin-right v-margin-bottom v-margin-left");
	pcontent.appendChild(mainvl);

	var mainvlexp = document.createElement("div");
	mainvlexp.setAttribute("style", "padding-top: 7px;");
	mainvlexp.setAttribute("class", "v-expand");
	mainvl.appendChild(mainvlexp);

	var mainvlexpslot = document.createElement("div");
	mainvlexpslot.setAttribute("style", "height: 90%; margin-top: -7px;");
	mainvlexpslot.setAttribute("class", "v-slot");
	mainvlexp.appendChild(mainvlexpslot);

	var tvl = document.createElement("div");
	tvl.setAttribute("style", "height: 100%; width: 100%;");
	tvl.setAttribute("class","v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height");
	mainvlexpslot.appendChild(tvl);

	var tvlexp = document.createElement("div");
	tvlexp.setAttribute("style", "padding-top: 0px;");
	tvlexp.setAttribute("class", "v-expand");
	tvl.appendChild(tvlexp);

	var tvlexpslot = document.createElement("div");
	tvlexpslot.setAttribute("style", "height: 100%; margin-top: 0px;");
	tvlexpslot.setAttribute("class", "v-slot");
	tvlexp.appendChild(tvlexpslot);

	var table = document.createElement("div");
	table.setAttribute("style", "height: 100%; width: 100%;");
	table.setAttribute("class", "v-table v-widget v-has-width v-has-height");
	tvlexpslot.appendChild(table);

	var tableheadwrap = document.createElement("div");
	tableheadwrap.setAttribute("style", "width: 758px;");
	tableheadwrap.setAttribute("class", "v-table-header-wrap");
	tableheadwrap.setAttribute("aria-hidden", "false");
	table.appendChild(tableheadwrap);

	var tablehead = document.createElement("div");
	tablehead.setAttribute("style", "overflow: hidden;");
	tablehead.setAttribute("class", "v-table-header");
	tableheadwrap.appendChild(tablehead);

	var tablehead1 = document.createElement("div");
	tablehead1.setAttribute("style", "width: 900000px;");
	tablehead.appendChild(tablehead1);

	var tabletable = document.createElement("table");
	tablehead1.appendChild(tabletable);

	var tabletablebody = document.createElement("tbody");
	tabletable.appendChild(tabletablebody);

	var tabletablebodytr = document.createElement("tr");
	tabletablebody.appendChild(tabletablebodytr);

	// Subject
	var tdsubject = document.createElement("td");
	tdsubject.style.width = constants.SUBJECT_COLUMN_WIDTH + 13 + "px";
	tdsubject.setAttribute("class", "v-table-header-cell");
	tabletablebodytr.appendChild(tdsubject);

	var tdsubjectr = document.createElement("div");
	tdsubjectr.setAttribute("class", "v-table-resizer");
	tdsubject.appendChild(tdsubjectr);

	var tdsubjectsi = document.createElement("div");
	tdsubjectsi.setAttribute("class", "v-table-sort-indicator");
	tdsubject.appendChild(tdsubjectsi);

	var tdsubjectcaption = document.createElement("div");
	tdsubjectcaption.style.width = constants.SUBJECT_COLUMN_WIDTH - 5 + "px";
	tdsubjectcaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
	tdsubjectcaption.textContent = "Выдан кому";
	tdsubject.appendChild(tdsubjectcaption);

	// Issuer
	var tdissuer = document.createElement("td");
	tdissuer.style.width = constants.ISSUER_COLUMN_WIDTH + 13 + "px";
	tdissuer.setAttribute("class", "v-table-header-cell");
	tabletablebodytr.appendChild(tdissuer);

	var tdissuerr = document.createElement("div");
	tdissuerr.setAttribute("class", "v-table-resizer");
	tdissuer.appendChild(tdissuerr);

	var tdissuercaption = document.createElement("div");
	tdissuercaption.style.width = constants.ISSUER_COLUMN_WIDTH - 5 + "px";
	tdissuercaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
	tdissuercaption.textContent = "Выдан кем";
	tdissuer.appendChild(tdissuercaption);

	// ValidFrom
	var tdvfrom = document.createElement("td");
	tdvfrom.style.width = constants.VALIDDATE_COLUMN_WIDTH + 13 + "px";
	tdvfrom.setAttribute("class", "v-table-header-cell");
	tabletablebodytr.appendChild(tdvfrom);

	var tdvfromr = document.createElement("div");
	tdvfromr.setAttribute("class", "v-table-resizer");
	tdvfrom.appendChild(tdvfromr);

	var tdvfromsi = document.createElement("div");
	tdvfromsi.setAttribute("class", "v-table-sort-indicator");
	tdvfrom.appendChild(tdvfromsi);

	var tdvfromcaption = document.createElement("div");
	tdvfromcaption.style.width = constants.VALIDDATE_COLUMN_WIDTH - 5 + "px";
	tdvfromcaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
	tdvfromcaption.textContent = "Действителен с";
	tdvfrom.appendChild(tdvfromcaption);

	// ValidTo
	var tdvto = document.createElement("td");
	tdvto.style.width = constants.VALIDDATE_COLUMN_WIDTH + 13 + "px";
	tdvto.setAttribute("class", "v-table-header-cell");
	tabletablebodytr.appendChild(tdvto);

	var tdvtor = document.createElement("div");
	tdvtor.setAttribute("class", "v-table-resizer");
	tdvto.appendChild(tdvtor);

	var tdvtosi = document.createElement("div");
	tdvtosi.setAttribute("class", "v-table-sort-indicator");
	tdvto.appendChild(tdvtosi);

	var tdvtocaption = document.createElement("div");
	tdvtocaption.style.width = constants.VALIDDATE_COLUMN_WIDTH - 5 + "px";
	tdvtocaption.setAttribute("class", "v-table-caption-container v-table-caption-container-align-left");
	tdvtocaption.textContent = "Действителен по";
	tdvto.appendChild(tdvtocaption);

	// Table body
	var tablebodywrap = document.createElement("div");
	tablebodywrap.setAttribute("style", "zoom: 1; position: relative; overflow: auto; height: 278px; width: 758px;");
	tablebodywrap.setAttribute("class", "v-scrollable v-table-body-wrapper v-table-body");
	tablebodywrap.setAttribute("tabindex", "-1");
	table.appendChild(tablebodywrap);

	var tablebodywrapns = document.createElement("div");
	tablebodywrapns.setAttribute("id", constants.TABLE_CONTENT);
	tablebodywrapns.setAttribute("style", "height: 20px;");
	tablebodywrapns.setAttribute("class", "v-table-body-noselection");
	tablebodywrap.appendChild(tablebodywrapns);

	var tablebodywrapnsrs = document.createElement("div");
	tablebodywrapnsrs.setAttribute("style", "height: 0px;");
	tablebodywrapnsrs.setAttribute("class", "v-table-row-spacer");
	tablebodywrapns.appendChild(tablebodywrapnsrs);

	var bodyTable = document.createElement("table");
	bodyTable.setAttribute("class", "v-table-table");
	tablebodywrapns.appendChild(bodyTable);

	var bodyTableBody = document.createElement("tbody");
	bodyTableBody.setAttribute("id", constants.CERT_TABLE_BODY);// /Вот сюда насыпать будем строки сертов
	bodyTableBody.setAttribute("class", "v-table-table");
	bodyTable.appendChild(bodyTableBody);

	var rs = document.createElement("div");
	rs.setAttribute("style", "height: 0px;");
	rs.setAttribute("class", "v-table-row-spacer");
	tablebodywrapns.appendChild(rs);

	var tablebodywrap1 = document.createElement("div");
	tablebodywrap1.setAttribute("style", "position: fixed; top: 0px; left: 0px;");
	tablebodywrap1.setAttribute("tabindex", "0");
	tablebodywrap.appendChild(tablebodywrap1);

	// tablebodywrap closed

	var tablefootwrap = document.createElement("div");
	tablefootwrap.setAttribute("style", "display: none; width: 758px;");
	tablefootwrap.setAttribute("class", "v-table-footer-wrap");
	tablefootwrap.setAttribute("aria-hidden", "true");
	table.appendChild(tablefootwrap);

	var footer = document.createElement("div");
	footer.setAttribute("style", "overflow: hidden;");
	footer.setAttribute("class", "v-table-footer");
	tablefootwrap.appendChild(footer);

	var footer1 = document.createElement("div");
	footer1.setAttribute("style", "width: 900000px;");
	footer.appendChild(footer1);

	var ftable = document.createElement("table");
	footer1.appendChild(ftable);

	var ftablebody = document.createElement("tbody");
	ftable.appendChild(ftablebody);

	var ftablebodytr = document.createElement("tr");
	ftablebody.appendChild(ftablebodytr);

	// footer td1
	var footertd1 = document.createElement("td");
	footertd1.setAttribute("style", "width: 183px;");
	ftablebodytr.appendChild(footertd1);

	var footertd1div = document.createElement("div");
	footertd1div.setAttribute("style", "width: 170px;");
	footertd1div.setAttribute("class", "v-table-footer-container");
	footertd1div.textContent = "&nbsp";
	footertd1.appendChild(footertd1div);

	// footer td2
	var footertd2 = document.createElement("td");
	footertd2.setAttribute("style", "width: 162px;");
	ftablebodytr.appendChild(footertd2);

	var footertd2div = document.createElement("div");
	footertd2div.setAttribute("style", "width: 149px;");
	footertd2div.setAttribute("class", "v-table-footer-container");
	footertd2div.textContent = "&nbsp";
	footertd2.appendChild(footertd2div);

	// footer td3
	var footertd3 = document.createElement("td");
	footertd3.setAttribute("style", "width: 216px;");
	ftablebodytr.appendChild(footertd3);

	var footertd3div = document.createElement("div");
	footertd3div.setAttribute("style", "width: 203px;");
	footertd3div.setAttribute("class", "v-table-footer-container");
	footertd3div.textContent = "&nbsp";
	footertd3.appendChild(footertd3div);

	// footer td4
	var footertd4 = document.createElement("td");
	footertd4.setAttribute("style", "width: 176px;");
	ftablebodytr.appendChild(footertd4);

	var footertd4div = document.createElement("div");
	footertd4div.setAttribute("style", "width: 163px;");
	footertd4div.setAttribute("class", "v-table-footer-container");
	footertd4div.textContent = "&nbsp";
	footertd4.appendChild(footertd4div);

	var spacing = document.createElement("div");
	spacing.setAttribute("class", "v-spacing");
	mainvlexp.appendChild(spacing);

	var btnSlot = document.createElement("div");
	btnSlot.setAttribute("style", "height: 10%;");
	btnSlot.setAttribute("class", "v-slot");
	mainvlexp.appendChild(btnSlot);

	var btnLayout = document.createElement("div");
	btnLayout.setAttribute("style", "height: 100%; width: 100%;");
	btnLayout.setAttribute("class", "v-verticallayout v-layout v-vertical v-widget v-has-width v-has-height");
	btnSlot.appendChild(btnLayout);

	var btnLayoutExp = document.createElement("div");
	btnLayoutExp.setAttribute("style", "padding-top: 0px;");
	btnLayoutExp.setAttribute("class", "v-expand");
	btnLayout.appendChild(btnLayoutExp);

	var btnLayoutExpSlot = document.createElement("div");
	btnLayoutExpSlot.setAttribute("style", "height: 100%; margin-top: 0px;");
	btnLayoutExpSlot.setAttribute("class", "v-slot v-align-center v-align-middle");
	btnLayoutExp.appendChild(btnLayoutExpSlot);

	var btnLayoutExpSlotDiv = document.createElement("div");
	btnLayoutExpSlotDiv.setAttribute("role", "button");
	btnLayoutExpSlotDiv.setAttribute("class", "v-button v-widget");
	btnLayoutExpSlotDiv.setAttribute("tabindex", "0");
	btnLayoutExpSlot.appendChild(btnLayoutExpSlotDiv);
	btnLayoutExpSlotDiv.addEventListener("mouseup", function() {
		closeChooser();
	}, false);

	var btnLayoutExpSlotDivSpan = document.createElement("span");
	btnLayoutExpSlotDivSpan.setAttribute("class", "v-button-wrap");
	btnLayoutExpSlotDiv.appendChild(btnLayoutExpSlotDivSpan);

	var btnLayoutExpSlotDivSpanSpan = document.createElement("span");
	btnLayoutExpSlotDivSpanSpan.setAttribute("class", "v-button-caption");
	btnLayoutExpSlotDivSpan.appendChild(btnLayoutExpSlotDivSpanSpan);
	btnLayoutExpSlotDivSpanSpan.textContent = "Отмена";

	var decor = document.createElement("div");
	decor.setAttribute("style", "margin-bottom: -9px;");
	decor.setAttribute("class", "v-panel-deco");
	certChooser.appendChild(decor);

	var d = document.getElementById('signForm');
	d.appendChild(certChooser);

	Common_FillCertList(constants.CERT_TABLE_BODY);
}

/**
 * Заполнение списка сертификатов, доступных для подписания
 * @param certsTableId
 */
function Common_FillCertList(certsTableId) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return NewFillCertList_Async(certsTableId);
		});
	} else {
		return NewFillCertList_NPAPI(certsTableId);
	}
}

/**
 * Закрывает окно выбора сертификатов
 */
function closeChooser() {
	signForm = document.getElementById('signForm');
	if (signForm != null) {
		chooser = document.getElementById(constants.CERT_CHOOSER);
		if (chooser != null) {
			signForm.removeChild(chooser);
		} else {
			log("Cannot find chooser form!");
		}
	} else {
		log("Cannot find sign form!");
	}
}

function log(message) {
	console.info(message);
	// ru.alfabank.orrpp.jscallback.log(message);

}

function logerror(message) {
	console.log(message);
	// ru.alfabank.orrpp.jscallback.showError(message);
}

function addCertificateOption(certTableBody, certThumbprint, subjectSimpleName, issuerSimpleName, validFromDate, validToDate, isValid) {

	var tbody = document.getElementById(certTableBody);

	if (!tbody) {
		logerror("ERROR! Cannot find element with id: " + certTableBody);
		return;
	}

	var tr = document.createElement("tr");
	if (oddRow) {
		tr.setAttribute("class", "v-table-row");
	} else {
		tr.setAttribute("class", "v-table-row-odd");
	}
	if(!isValid && highlightInvalidCertificates){
		tr.setAttribute("style", "background-color: rgb(255, 179, 179);");
	}
	
	oddRow = !oddRow;
	tbody.appendChild(tr);

	var td = document.createElement("td");
	td.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
	td.style.width = constants.SUBJECT_COLUMN_WIDTH + "px";
	td.setAttribute("class", "v-table-cell-content");
	tr.appendChild(td);

	var tddiv = document.createElement("div");
	tddiv.style.width = constants.SUBJECT_COLUMN_WIDTH + "px";
	tddiv.setAttribute("class", "v-table-cell-wrapper");
	tddiv.textContent = subjectSimpleName;
	td.appendChild(tddiv);

	var td1 = document.createElement("td");
	td1.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
	td1.style.width = constants.ISSUER_COLUMN_WIDTH + "px";
	td1.setAttribute("class", "v-table-cell-content");
	tr.appendChild(td1);

	var tddiv1 = document.createElement("div");
	tddiv1.style.width = constants.ISSUER_COLUMN_WIDTH + "px";
	tddiv1.setAttribute("class", "v-table-cell-wrapper");
	tddiv1.textContent = issuerSimpleName;
	td1.appendChild(tddiv1);

	var td2 = document.createElement("td");
	td2.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
	td2.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
	td2.setAttribute("class", "v-table-cell-content");
	tr.appendChild(td2);

	var tddiv2 = document.createElement("div");
	tddiv2.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
	tddiv2.setAttribute("class", "v-table-cell-wrapper");
	tddiv2.textContent = formatDate(validFromDate);
	td2.appendChild(tddiv2);

	var td3 = document.createElement("td");
	td3.setAttribute("style", "cursor: pointer; color: rgb(27, 105, 159);");
	td3.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
	td3.setAttribute("class", "v-table-cell-content");
	tr.appendChild(td3);

	var tddiv3 = document.createElement("div");
	tddiv3.style.width = constants.VALIDDATE_COLUMN_WIDTH + "px";
	tddiv3.setAttribute("class", "v-table-cell-wrapper");
	tddiv3.textContent = formatDate(validToDate);
	td3.appendChild(tddiv3);

	tr.addEventListener("mouseup", function() {
		closeChooser();
		var setDisplayData;
		var hashToSign="92CD0CB36B10BFB88DEF198F80B7D2E667DBDA064D346405C25EEF77FFE375D7";//debug
		NewCommon_SignCadesBES(certThumbprint, hashToSign, setDisplayData);
	}, false);

}

/**
 * Новый метод для запуска подписания хэша
 * @param certThumbprint - отпечаток сертификата, связанного с выбранным приватным ключом
 * @param hashToSign - хэш документа, который подписываем.
 * @param setDisplayData
 */
function NewCommon_SignCadesBES(certThumbprint, hashToSign, setDisplayData) {
	var canAsync = !!cadesplugin.CreateObjectAsync;
	if (canAsync) {
		include_async_code().then(function() {
			return NewSignHashCadesBES_Async(certThumbprint, hashToSign, setDisplayData);
		});
	} else {
		return NewSignHashCadesBES_NPAPI(certThumbprint, hashToSign, setDisplayData);
	}
}

/**
 * Форматирование даты по ГОСТ Р 6.30-2003
 * 
 * @param date
 * @returns {String}
 */
function formatDate(date) {
	var nd = date;
	if (isIE()) {
		nd = new Date(date);
	}
	var d = nd.getDate();
	var m = nd.getMonth() + 1;
	// 0-based
	var y = nd.getFullYear();
	var hh = nd.getHours();
	var mm = nd.getMinutes();
	var ss = nd.getSeconds();
	return (d < 10 ? "0" + d : d) + "." + (m < 10 ? "0" + m : m) + "." + y
			+ " " + (hh < 10 ? "0" + hh : hh) + ":" + (mm < 10 ? "0" + mm : mm)
			+ ":" + (ss < 10 ? "0" + ss : ss);

}

/**
 * Заполнение списка сертификатов, доступных для подписания
 * @param certsTableId
 */
function NewFillCertList_NPAPI(certsTableId) {
	try {
		var oStore = cadesplugin.CreateObject("CAdESCOM.Store");
		oStore.Open();
	} catch (ex) {
		alert("Ошибка при открытии хранилища: " + GetErrorMessage(ex));
		return;
	}
	try {
		var lst = document.getElementById(certsTableId);
		if (!lst){
			return;
		}
	} catch (ex) {
		return;
	}

	var certCnt = 0;
	try {
		certCnt = oStore.Certificates.Count;
		if (certCnt == 0){
			throw "Cannot find object or property. (0x80092004)";
		}
	} catch (ex) {
		var message = GetErrorMessage(ex);
		if ("Cannot find object or property. (0x80092004)" == message
				|| "oStore.Certificates is undefined" == message
				|| "Объект или свойство не найдено. (0x80092004)" == message) {
			oStore.Close();
			var errormes = document.getElementById("boxdiv").style.display = '';
			return;
		}
	}
	log("Found " + certCnt + " certificates.");
	// Увеличиваю высоту для контента таблицы

	var tcontent = document.getElementById(certsTableId);
	tcontent.style.height = (20 * certCnt) + "px";

	for (var i = 1; i <= certCnt; i++) {
		var cert;
		try {
			cert = oStore.Certificates.Item(i);
		} catch (ex) {
			alert("Ошибка при перечислении сертификатов: "	+ GetErrorMessage(ex));
			return;
		}

		var certThumbprint;
		var validFromDate;
		var validToDate;
		var isValid;

		var dateObj = new Date();
		try {
			if (dateObj < cert.ValidToDate && dateObj > cert.ValidFromDate	&& cert.HasPrivateKey()) {
				certThumbprint = cert.Thumbprint;
            	validToDate = new Date((cert.ValidToDate));
            	validFromDate = new Date((cert.ValidFromDate));
            	isValid = cert.IsValid;
	            	
            	var certificate = GetCertificateByThumbprint_NPAPI(certThumbprint);            	
            	var certObj = new CertificateObj(certificate);
				addCertificateOption(certsTableId, certThumbprint, certObj.GetCertName(), certObj.GetIssuer(), validFromDate, validToDate, isValid);
			} 
		} catch (ex) {
			alert("Ошибка при получении свойств сертификата: "	+ GetErrorMessage(ex));
		}
	}
	oStore.Close();
}

/**
 * Пождписание для IE 9+
 * @param certId
 * @param hashToSign
 * @param setDisplayData
 */
function NewSignHashCadesBES_NPAPI(certId, hashToSign, setDisplayData) {
	var certificate = GetCertificateByThumbprint_NPAPI(certId);
	var x = GetSignatureTitleElement();
	try {
		FillCertInfo_NPAPI(certificate);
		var signature = MakeCadesBesSignHash_NPAPI(hashToSign, certificate,	setDisplayData);
		document.getElementById("SignatureTxtBox").innerHTML = signature;
		if (x != null) {
			x.innerHTML = "Подпись сформирована успешно:";
		}
	} catch (err) {
		if (x != null) {
			x.innerHTML = "Возникла ошибка:";
		}
		document.getElementById("SignatureTxtBox").innerHTML = err;
	}
}


/**
 * Подписывает и возвращает CMS.
 *
 * TODO удалить сертификаты из CMS
 *
 * @param certSHA1Hash - хэш сертификата, для поиска в хранилище
 * @param hashToSign - данные, закодированные в base64.
 * @returns CMS(ContentInfo)
 */
function MakeCadesBesSignHash_NPAPI(hashToSign, certObject, setDisplayData) {
	var errormes = "";

    // Алгоритм хэширования, при помощи которого было вычислено хэш-значение
    // Полный список поддерживаемых алгоритмов указан в перечислении
    // CADESCOM_HASH_ALGORITHM
    var hashAlg = cadesplugin.CADESCOM_HASH_ALGORITHM_CP_GOST_3411; // ГОСТ Р 34.11-94

    // Создаем объект CAdESCOM.HashedData
    var oHashedData = initializeHashedData(hashAlg, hashToSign);

	
	try {
		var oSigner = cadesplugin.CreateObject("CAdESCOM.CPSigner");
	} catch (err) {
		errormes = "Failed to create CAdESCOM.CPSigner: " + err.number;
		alert(errormes);
		throw errormes;
	}

	if (oSigner) {
		oSigner.Certificate = certObject;
	} else {
		errormes = "Failed to create CAdESCOM.CPSigner";
		alert(errormes);
		throw errormes;
	}

	try {
		var oSignedData = cadesplugin.CreateObject("CAdESCOM.CadesSignedData");
	} catch (err) {
		alert('Failed to create CAdESCOM.CadesSignedData: ' + err.number);
		return;
	}

	var CADES_BES = 1;
	var Signature;

	if (hashToSign) {
		// Данные на подпись ввели
		oSignedData.ContentEncoding = 1; // CADESCOM_BASE64_TO_BINARY
		if (typeof (setDisplayData) != 'undefined') {
			// Set display data flag flag for devices like Rutoken PinPad
			oSignedData.DisplayData = 1;
		}
		oSigner.Options = 1; // CAPICOM_CERTIFICATE_INCLUDE_WHOLE_CHAIN
		try {
			Signature = oSignedData.SignHash(oHashedData, oSigner, CADES_BES);
		} catch (err) {
			errormes = "Не удалось создать подпись из-за ошибки: "
					+ GetErrorMessage(err);
			alert(errormes);
			throw errormes;
		}
	}
	return Signature;
}

/**
 * Инициализация объекта
 */
function initializeHashedData(hashAlg, sHashValue) {
    // Создаем объект CAdESCOM.HashedData
    var oHashedData = cadesplugin.CreateObject("CAdESCOM.HashedData");

    // Инициализируем объект заранее вычисленным хэш-значением
    // Алгоритм хэширования нужно указать до того, как будет передано хэш-значение
    oHashedData.Algorithm = hashAlg; 
    oHashedData.SetHashValue(sHashValue);

    return oHashedData;
}

function init() {
	document.getElementById("SignatureTxtBox").innerHTML = "";
	var canPromise = !!window.Promise;
	if (canPromise) {
		cadesplugin.then(function() {
			Common_CheckForPlugIn();
		}, function(error) {
			document.getElementById('PluginEnabledImg').setAttribute("src", "Img/red_dot.png");
			document.getElementById('PlugInEnabledTxt').innerHTML = error;
		});
	} else {
		window.addEventListener(
						"message",
						function(event) {
							if (event.data == "cadesplugin_loaded") {
								CheckForPlugIn_NPAPI();
							} else if (event.data == "cadesplugin_load_error") {
								document.getElementById('PluginEnabledImg').setAttribute("src", "Img/red_dot.png");
								document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин не загружен";
							}
						}, false);
		window.postMessage("cadesplugin_echo_request", "*");
	}
}